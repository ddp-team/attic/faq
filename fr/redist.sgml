<!-- Subversion revision of original English document "3548" -->

<chapt id="redistrib">Distribuer &debian; dans un produit commercial

<sect id="sellcds">Puis-je construire et vendre des c�d�roms Debian&nbsp;?

<p>Allez-y. Vous n'avez pas besoin de permission pour distribuer quelque chose 
que nous avons <em>publi�</em>, vous pouvez mast�riser votre c�d�rom d�s la 
fin des beta-tests. Et vous n'avez pas besoin de nous r�tribuer. Naturellement, 
tous les fabricants de c�d�rom doivent se conformer aux licences des programmes 
fournis par Debian. Par exemple, beaucoup de programmes sont distribu�s sous la 
licence GPL, qui vous oblige � fournir leur code source.

<p>En outre, nous publions une liste de fabricants de c�d�rom qui donnent de l'argent, 
des logiciels, et du temps au projet Debian, et nous encourageons les utilisateurs � 
acheter aupr�s de ces fabricants, les dons sont donc une bonne publicit�.

<sect id="packagednonfree">Debian peut-elle �tre empaquet�e avec des logiciels non-libres&nbsp;?

<p>Oui. Bien que tous les composants principaux de Debian soient des logiciels 
libres, nous fournissons un r�f�rentiel �&nbsp;non-free&nbsp;� pour les programmes qui ne
sont pas librement redistribuables.

<p>Les fabricants de c�d�roms <em>peuvent</em> distribuer les programmes que nous 
avons plac�s dans ce r�f�rentiel, selon les termes des licences ou des accords priv�s 
conclus avec les auteurs de ces logiciels.
Ils peuvent aussi distribuer des logiciels non libres provenant d'autres sources sur le
m�me c�d�rom. Cela n'est pas nouveau, des logiciels libres et des logiciels commerciaux
sont maintenant distribu�s sur le m�me c�d�rom par beaucoup de fabricants.
Naturellement nous encourageons toujours les auteurs de logiciel � distribuer leurs programmes 
en tant que logiciels libres.

<sect id="childistro">Je fais une distribution Linux particuli�re pour �&nbsp;un march� vertical&nbsp;�.
Puis-je utiliser &debian; comme base du syst�me et ajouter mes propres applications au-dessus&nbsp;?

<p>Oui. Par exemple, quelqu'un construit une distribution �&nbsp;Linux for Hams&nbsp;, avec des programmes 
sp�cialis�s pour les radio amateurs. Il installe d'abord Debian comme �&nbsp;syst�me de base&nbsp;�, et 
ajoute des programmes pour commander l'�metteur, pour pister les satellites, etc.
Tous les paquets ajout�s ont �t� cr��s avec le syst�me de paquet de Debian,  
de sorte que ses utilisateurs peuvent facilement mettre � jour leur syst�me avec la version du
c�d�rom suivant.

<p>Il y a d�j� plusieurs distributions d�riv�es de Debian sur le march�, comme Corel Linux 
et Storm Linux, qui s'adressent � un public diff�rent de Debian mais elles utilisent la 
plupart de nos composants dans leur produit.

<p>Debian fournit �galement un m�canisme pour permettre aux d�veloppeurs et aux administrateurs 
syst�me d'installer des versions locales de fichiers de telle mani�re que ces fichiers ne soient
pas �cras�s quand d'autres paquets sont mis � jour. Ceci est discut� dans la question&nbsp;: 
<ref id="divert">.

<sect id="commercialdebs">Est-ce que je peux cr�er un paquet Debian pour mon programme commercial 
				de sorte qu'il s'installe facilement sur n'importe quel syst�me de Debian&nbsp;?

<p>Bien sur. L'outil de gestion des paquets est un logiciel libre. Les paquets peuvent �tre libres ou non, il 
peut les installer tous.

<!--
We should probably recommend here to follow debian-policy,
so that the commercial package fits with the rest of the system.
-->
